features:
  top:
    - name: "サーバレス (アルファ版)"
      name_en: "Serverless (alpha)"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/project/clusters/serverless/"
      image_url: "/images/11_6/serverless.png"
      reporter: danielgruesso
      stage: configure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/43959"
      description: |
        [GitLab 11.5で導入されたKnative連携機能](/2018/12/13/gitlab-11-5-released/#easily-deploy-and-integrate-knative-with-gitlab)
        を拡張して、関数の定義を容易にするサーバレス機能が追加されました。
        定義した関数はKnativeによって管理されます。

        リポジトリ内の`serverless.yml`と`.gitlab-ci.yml`を使用して関数を定義することによって、
        Kubernetesクラスタに関数をデプロイできます。
        関数は、リクエストのボリュームに従い、Knativeが自動的にスケールします。
        これにより、アプリケーション開発者はインフラのプロビジョニングや管理について気にせずに、
        開発に集中することができるので開発スピードが向上します。

  primary:
    - name: "マージリクエストでCI/CDを実行"
      name_en: "Run CI/CD for merge requests"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: true
      documentation_link: "https://docs.gitlab.com/ee/ci/merge_request_pipelines/index.html"
      image_url: "/images/11_6/verify-mergerequestpipelines.png"
      reporter: jlenny
      stage: verify
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/15310"
      description: |
        マージリクエストのコンテキストでジョブを実行することが容易になりました。
        `only/except`キーワードで`merge_requests`を指定することで、
        マージリクエストのコンテキストのみで(または除いて)ジョブを実行できます。
        これにより、パイプラインできめ細やかなジョブの制御が可能になります。
        また、target branchやマージリクエストIDなどの環境変数にアクセスできることで、
        より複雑な振る舞いのジョブを実装することもできます。

    - name: "変更の提案"
      name_en: "Suggested Changes"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/discussions/#suggest-changes"
      video: 'https://www.youtube.com/embed/LUG8H46sXCI'
      image_url: "/images/11_6/suggested-change.png"
      reporter: jramsay
      stage: create
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/18008"
      description: |
        これからは、提案された変更を受け入れるために、コピー&ペーストする必要はありません。
        マージリクエストの差分で提案された変更を、1クリックで受け入れることができます。

        マージリクエストの差分にコメントで変更の提案をすることができ、
        source branchに書き込み権限のあるユーザは誰でも変更を受け入れることができます。

        この機能はGitLab.comでは有効になっていますが、
        セルフホストのGitLabインスタンスで使用するには`diff_suggetions`
        [フィーチャーフラグ](https://docs.gitlab.com/ee/api/features.html){:target="_blank"}
        で有効化する必要があります。
        セルフホストのGitLabインスタンスでは、11.7でデフォルトで有効化される予定です。

    - name: "Web IDE用のWeb Terminal (ベータ版)"
      name_en: "Web Terminal for Web IDE (beta)"
      available_in: [ultimate]
      gitlab_com: false
      documentation_link: "https://docs.gitlab.com/ee/user/project/web_ide/index.html#interactive-web-terminals-for-the-web-ide-ultimate-only"
      image_url: "/images/11_6/web-terminal.png"
      reporter: jramsay
      stage: create
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/5426"
      description: |
        Web IDEはローカル環境でbranchを移動したり、stashしたりすることなく、簡単に
        そして早く変更の実装やマージリクエストのフィードバックを完結させてくれます。
        しかし、テストを実行するターミナル、REPLでのテストコードのコンパイル、より大掛かりな変更の
        際はターミナルがないと難しいです。

        Web IDEからAPIレスポンスを調査したり、REPLで構文チェックをしたりするのに、ローカル環境でやっているように
        エディタとターミナルを並べて表示できるようになりました。
        Web TerminalはWeb IDEの初めてのサーバー側の評価機能であり、新しい`.gitlab-webide.yml`ファイルで定義することができます。
        対話式のWeb TerminalはGitlab.comではまだ利用できません。
        [こちらの課題](https://gitlab.com/gitlab-org/gitlab-ce/issues/52611){:target="_blank"}
        で進捗状況がわかります。
        現在のところ、変更はエディター側とターミナル側で同期されません。
        次回以降のリリースで、
        [変更のミラーリング](https://gitlab.com/gitlab-org/gitlab-ee/issues/5276){:target="_blank"}
        と
        [ライブ・プレビュー](https://gitlab.com/gitlab-org/gitlab-ee/issues/4013){:target="_blank"}
        をサポートする予定です。

    - name: "グループ用のプロジェクトテンプレート"
      name_en: "Project templates for Groups"
      available_in: [premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/group/custom_project_templates.html"
      image_url: "/images/11_6/group-level-templates.png"
      reporter: akaemmerle
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/6861"
      description: |
        プロジェクトテンプレートは、新規に、素早くプロジェクトを立ち上げたいときに役立ちます。
        [11.2](https://about.gitlab.com/2018/08/22/gitlab-11-2-released/#custom-project-templates-on-the-instance-level){:target="_blank"}
        では、
        [インスタンスレベル](https://gitlab.com/gitlab-org/gitlab-ce/issues/48043){:target="_blank"}
        でのプロジェクトテンプレートを実装しました。

        11.6では、グループでも同様の機能をリリースでき、非常に嬉しく思います。
        テンプレートからプロジェクトを作成する画面に追加された、グループタブにあるテンプレートを選択してプロジェクトを作成できます。
        これにより、設定作業は合理化され、特にマイクロサービスアーキテクチャなどのより大きなグループ構造を持つときに
        一貫性を保つことができます。

    - name: "グループ用のKubernatesクラスタ (ベータ版)"
      name_en: "Kubernetes clusters for Groups (beta)"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/group/clusters/"
      image_url: "/images/11_6/group-clusters.png"
      reporter: danielgruesso
      stage: configure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/34758"
      description: |
        しばしば関連するプロジェクトで作業する開発チームは、アプリをデプロイするために
        同じk8sクラスタを利用する必要があることがあります。
        11.6を利用することで、グループやサブグループ配下の全てのプロジェクトが利用できる、
        グループレベルのk8sクラスタを作成することができます。

        これで、プロジェクト内で環境を構築するために必要だった労力と時間を削減することができ、
        優れたアプリケーションの開発に注力できるようになるでしょう。

    - name: "Kubernetesの証明書管理"
      name_en: "Cert-manager for Kubernetes"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/project/clusters/#installing-applications"
      image_url: "/images/11_6/cert-manager.png"
      reporter: danielgruesso
      stage: configure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/40635"
      description: |
        アプリケーションを安全にすることは製品レベルのデプロイをするうえで必須となります。
        Cert-managerはk8sネイティブの証明書管理コントローラであり、自動的にLet's Encryptの証明書を発行、
        更新してくれます。

        SSL証明書を利用することで、KubernetesにデプロイしたJupiterHub、
        およびAuto DevOpsで作成したアプリケーションへの接続にHTTPSを利用することができます。

    - name: "グループセキュリティダッシュボードで脆弱性チャートを提供"
      name_en: "Vulnerability Chart for Group Security Dashboards"
      available_in: [ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/group/security_dashboard/#viewing-the-vulnerabilities"
      image_url: "/images/11_6/dashboard-metrics.png"
      reporter: bikebilly
      stage: secure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/6954"
      description: |
        [グループセキュリティダッシュボード](https://docs.gitlab.com/ee/user/group/security_dashboard/){:target="_blank"}
        はセキュリティの専門家がプロジェクト内の脆弱性を把握できる主要なツールです。
        日々、どれくらいの脆弱性が変化しているかを知り、チームが素早く問題を解決できているかを知ることが、
        最も重要な要件の一つとなります。

        GitLab 11.6では、グループセキュリティダッシュボードの脆弱性チャートで先月の脆弱性のグラフを簡単に表示できます。
        それぞれの重大度ごとに、脆弱性の値を読み、グラフ上を移動して特定の時点に関する詳細を確認できます。


  secondary:
    - name: "スマートカードハードウェアトークンでの認証"
      name_en: "Authenticate with a smart card hardware token"
      available_in: [premium, ultimate]
      gitlab_com: false
      documentation_link: "https://docs.gitlab.com/ee/administration/auth/smartcard.html"
      reporter: jeremy
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/726"
      description: |
        X.509セキュリティ証明書を使ったハードウェアトークンや、スマートカードの認証能力
        (YubiKeysや有名なアクセスカードなど)を使う環境にある組織向けにGitLabはローカルユーザの作成とログインを
        サポートします。

        ユーザはGitLabへのアクセスにハードウェアトークンを利用することができます。
        セキュリティが向上し、物理トークンに接続されていないユーザー名/パスワードの管理から解放されます。


    - name: "GitLab.comのグループのサブスクリプション詳細を表示"
      name_en: "Subscription details for Groups on GitLab.com"
      available_in: [free, bronze, silver, gold]
      documentation_link: "https://about.gitlab.com/gitlab-com/"
      image_url: "/images/11_6/billing_table.png"
      reporter: jeremy
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/7772"
      description: |
        GitLab.comの
        [有料プラン](https://about.gitlab.com/2017/04/11/introducing-subscriptions-on-gitlab-dot-com/){:target="_blank"}
        を契約している場合に、利用者自身でどのプランを契約しているかを簡単に把握できるようにしたいと考えました。

        バージョン11.6では、グループの設定ページの下部にある「請求」セクションでプランの詳細を表示するように改善しました。
        これで、あなたの現在および過去のプランや、サブスクリプションの利用開始日、終了日を把握することができます。

    - name: "Discordからの通知"
      name_en: "Discord notifications"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/project/integrations/discord_notifications.html"
      image_url: "/images/11_6/discord-notifications.png"
      reporter: victorwu
      stage: plan
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/21635"
      description: |
        今回のリリースで、GitLabで
        [Discord](https://discordapp.com/){:target="_blank"}
        連携が利用できるようになり、
        GitLabのイベントのレスポンスとしてDiscordのチャンネルにレポジトリへのpushや課題の更新、マージリクエストなどの
        通知を送ることができるようになります。

        [Vitaliy Klachkov](https://gitlab.com/blackst0ne){:target="_blank"}
        さんの貢献に感謝します。

    - name: "課題をエピックに昇進"
      name_en: "Promote issue to an epic"
      available_in: [ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/group/epics/#promoting-an-issue-to-an-epic"
      image_url: "/images/11_6/promote-to-epic.png"
      reporter: victorwu
      stage: plan
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/7730"
      description: |
        ソフトウェア開発はチーム全体を巻き込むクリエイティブなプロセスであり、チームメンバからのアイデアは歓迎されるべきです。
        課題として持ち上がったアイデアは、自由に新しい課題作成機能で、エピックに含めることができるようになります。

        新しい
        [クイック・アクション](https://docs.gitlab.com/ee/user/project/quick_actions.html){:target="_blank"}
        を利用して、容易に課題をエピックに含めることができます。

        課題のコメント欄で/promoteと入力し、コメントボタンをクリックします。
        これにより課題をクローズし、課題のプロジェクトの親グループに課題の内容を新しいエピックとしてコピーします。
        タイトル、概要、コメントスレッドに加えて、ラベル、参加者、いいね、等の情報も新しく作成されるエピックにコピーされます。

    - name: "課題とマージリクエストダッシュボードフィルタの改善"
      name_en: "Improved issue and merge request dashboard filtering"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/search/"
      image_url: "/images/11_6/issue-dashboard-search-filter-bar-design.png"
      reporter: victorwu
      stage: plan
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/52385"
      description: |
        課題とマージリクエストダッシュボードの検索バーのデザインを、ほかのGitLabの検索バーに合わせて更新しました。

    - name: "ユーザーごとの課題、マージリクエスト、エピックのソート順を保存"
      name_en: "Per-user saved sort order in issues, merge requests, and epics"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/group/epics/#searching-for-an-epic-from-epics-list-page"
      image_url: "/images/11_6/sort-order.png"
      reporter: victorwu
      stage: plan
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/39849"
      description: |
        ユーザーが設定した課題、マージリクエスト、エピックのソート方法があると思います。ロードマップのビューについてもあるかもしれません。
        どんなソートのタイプを選択したか、またどんなソート順を選択したか(昇順、降順）がシステムに保存されます。
        そのため、同じオブジェクトタイプのリスト画面にもどった時に、前回選択した設定が残されています。

    - name: "ロードマップでオープン、解決のエピックを表示"
      name_en: "View open or closed epics on roadmap"
      available_in: [ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/group/roadmap/"
      image_url: "/images/11_6/open-closed-epics-roadmap.png"
      reporter: victorwu
      stage: plan
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/8035"
      description: |
        先ごろエピックが完了した、またはエピックが関連が無くなったことを指し示す方法として、エピックをクローズする機能をリリースしました。
        このリリースに関連して、オープンのエピック、解決したエピック、もしくは両方をロードマップ上で表示する機能を提供します。
        残タスクや今後やるべきタスク(オープンなエピック)のみを注視したい、完了したタスク(解決したエピック)のレビューを行いたい、
        また現行のタスクに関連している最近完了したタスク(両方)を把握したいといったチームにとってとても便利です。
        この機能はそういった柔軟性を提供しています。加えて、あなたが選択した表示方法はユーザー毎にシステムに保存されるので、次回ロードマップを表示する際は、すでに選択された状態で表示されます。

    - name: "類似の課題"
      name_en: "Similar issues"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/project/issues/similar_issues.html"
      image_url: "/images/11_6/similar-issues.png"
      reporter: victorwu
      stage: plan
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/22071"
      description: |
        プロジェクトが大きくなり、多くの課題が作成されるようになると、同じような課題が繰り返し作成されることがあります。

        より早く回答が見つかるように、またメンテナの時間を無駄にしないように、新しい課題を作成する際に、内容の似た課題が表示されるようになります。
        とりわけ、課題作成のフォームのタイトルを入力する際に表示されます。
        これにより、ユーザーが類似した課題を即座に見ることができ、その課題に誘導し、既にある議論にすぐさま参加することができるようになります。

    - name: "公開鍵認証を使ったSSHでのpushミラーリングのサポート"
      name_en: "SSH push mirroring support with public-key authentication"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/workflow/repository_mirroring.html#pushing-to-a-remote-repository-core"
      reporter: jramsay
      stage: create
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/49565"
      description: |
        リポジトリミラーリングは、Gitリポジトリを別のロケーションに複製する機能です。
        これにより、GitLabにあるレポジトリから他のサーバへミラーリングをすることで、複数のGitLabインスタンスを作りやすくなります。
        しかし、公開鍵認証を使ったSSHでのGitアクセスのみを許可しているサーバもあったりします。
        GitLabでは、パスワード認証のSSH、HTTPでのPushミラーリングに加えて、公開鍵認証を利用したSSHでのPushミラーリングをサポートします。


    - name: "メンテナ権限でAPIを使ったパイプラインの削除が可能に"
      name_en: "Pipelines can now be deleted by project maintainers using API"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/api/pipelines.html#delete-a-pipeline"
      reporter: jlenny
      stage: verify
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/41875"
      description: |
        機密情報がパイプライン処理の中で漏洩してしまったり、たくさんの必要のないパイプラインが作成されていたり、
        パイプラインの削除が必要な課題が提起されたりする場合、APIを利用して、パイプラインを削除できるようになります。

    - name: "トリガー変数はデフォルトでUIで非表示に"
      name_en: "Trigger variables are now hidden in UI by default"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/ci/triggers/README.html#making-use-of-trigger-variables"
      image_url: "/images/11_6/verify-hidetriggervars.png"
      reporter: jlenny
      stage: verify
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/20422"
      description: |
        全てのトリガー変数は、UIで非表示になり、表示させるには手動の操作が必要となります。
        これで、スクリーンショットを撮ったり、画面共有の際に値が意図せず流出することを防ぎます。

    - name: "マージリクエストのレビュー時、メールでの通知を1回に"
      name_en: "Single email notification for Merge Request Reviews"
      available_in: [premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/discussions/index.html#merge-request-reviews-premium"
      reporter: jramsay
      stage: create
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/4326"
      description: |
        コードレビューは全ての成功しているプロジェクトにとって大切です。
        しかし、それぞれのコメントごとに通知を受け取ると大量になってしまいます。
        今後、レビューでは全てのフィードバックを1通のメールに集約して通知します。それにより、メールボックスがあふれることはないでしょう。

        GitLab 11.4で追加された
        [レビュー機能](/2018/11/02/gitlab-11-4-released/#merge-request-reviews){:target="_blank"}
        は、コードレビューを簡単にし、一つの操作でコメントを下書き/レビュー済み/対応済みにすることができるようになりました。
        この機能はGitLab.comではすでに有効化されていますが、セルフホストのGitLabインスタンスで利用するには
        [フィーチャーフラグ](https://docs.gitlab.com/ee/api/features.html){:target="_blank"}
        で有効化する必要があります。
        GitLab 11.7では、セルフホストのインスタンスでもデフォルトで有効化される予定です。

    - name: "プロジェクト概要の改善"
      name_en: "Improved project overview"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/project/"
      image_url: "/images/11_6/project-overview-ui.png"
      reporter: akaemmerle
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/51243"
      description: |
        GitLab 11.6では、プロジェクトの概要ページについて繰り返し検討しました。プロジェクトのヘッダーのバランスを改善し、
        余白部分の改善と、よく利用される操作を対比し目立たせています。また、全体的に情報の構造を改善しました。

    - name: "ユーザープロフィールのポップオーバー表示"
      name_en: "User profile popovers"
      available_in: [core, starter, premium, ultimate]
      image_url: "/images/11_6/user-centric-tooltip.png"
      reporter: akaemmerle
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/50157"
      description: |
        今回のリリースで、課題とマージリクエストページから、ユーザー名にマウスオーバーするとリッチなポップオーバー画面が表示されます。
        今まではフルネームのみの表示でしたが、今後は、フルネーム、ユーザーID、会社名、ロケーション情報、
        そして、もし有効化されていればステータスも表示されます。

        他のページにこの拡張機能を提供することに加え、
        [課題](https://gitlab.com/gitlab-org/gitlab-ce/issues/54915){:target="_blank"}
        と
        [マージリクエスト](https://gitlab.com/gitlab-org/gitlab-ce/issues/54916){:target="_blank"}
        に対して、続く機能拡張を開発しています。
        近いうちに、公開される予定です。

    - name: "マイルストーンとラベルのパンくずリストで「新規」、「編集」を表示"
      name_en: "Breadcrumb navigation shows 'New' and 'Edit' for milestones and labels"
      available_in: [core, starter, premium, ultimate]
      image_url: "/images/11_6/breadcrumb-new-edit.png"
      reporter: akaemmerle
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/43998"
      description: |
        今回のリリースで、GitLabはマイルストーンとラベルのパンくずリストの構造を強化しました。
        マイルストーンやラベルを新規作成、または編集する際に、
        状態に合わせてパンくずリストで”新規”、”編集”アイテムを表示するようになりました。
        これにより、課題やマージリクエストのUIとの一貫性が向上しました。

        [George Tsiolis](https://gitlab.com/gtsiolis){:target="_blank"}
        さんの貢献に感謝します。

    - name: "AutoDevOpsでHTTPSをサポート"
      name_en: "HTTPS Support for Auto DevOps"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/topics/autodevops/"
      reporter: danielgruesso
      stage: configure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/41355"
      description: |
        Auto DevOpsは質の高いソフトウェアをデリバリする際に起こる多くの難問の解決を目指しています。
        GitLab 11.6はHTTPSをサポートすることで、その能力を高めました。

        Kubernatesの証明書マネージャーを利用する際、Auto DevOpsは自動的にHTTPSを使ってアプリケーションを提供し、
        あなたのアプリケーションのセキュリティを向上させます。

    - name: "JupyterHubのHTTPS通信をサポート"
      name_en: "HTTPS support for JupyterHub"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/project/clusters/#installing-applications"
      reporter: danielgruesso
      stage: configure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/52753"
      description: |
        JupyterHub notebooksはデータチームに情報共有の強力な方法を提供します。
        しばしば、極秘データはさらに高いセキュリティを必要とします。

        Kubernatesの証明書マネージャーを利用する際、JupyterHubは自動的にHTTPS通信を使ってJupyterをデプロイし、あなたの極秘データにさらなるセキュリティを提供します。

    - name: "KubernatesのHTTPSレスポンスコードを表示"
      name_en: "Show Kubernetes HTTP response code"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/project/clusters/"
      reporter: danielgruesso
      stage: configure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/53628"
      description: |
        KubernatesのクラスタにGitLabが管理するアプリケーションをインストールする際のトラブルシューティングの助けになるよう、
        今回の改良で、KubernatesがHTTPレスポンスコードを返すようになりました。
        課題を解決するのがより早く、簡単になります。

    - name: "Goldプランで無料のゲストユーザが無制限に"
      name_en: "Unlimited free guests for Gold plans"
      available_in: [ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/permissions.html"
      reporter: jeremy
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ee/issues/6448"
      description: |
        バージョン11.0で、Ultimateプランでは無料のゲストユーザが無制限になりました。

        それをGoldプランにも拡大します。GitLab.comで最上位プランを利用中のグループ、セルフホスト、SaaSに関わらず、追加費用無しにゲストユーザを追加できるようになります。

    - name: "管理者ユーザによるユーザのなりすまし機能を無効に"
      name_en: "Disable impersonation of users by admins"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: "https://docs.gitlab.com/ee/api/README.html#disable-impersonation"
      reporter: jeremy
      stage: manage
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/40385"
      description: |
        組織によっては、管理者ユーザにユーザのなりすましを許可することは、管理者ユーザのその行動が、
        なりすまし対象のユーザによるものとみなされるため、セキュリティリスクを引き起こします。
        これに対処するために、管理者ユーザによるなりすまし機能を無効化する設定を追加しました。

    - name: "TOMLとJSONのマークダウンフロントマターフィルタリングが可能に"
      name_en: "Markdown front matter filtering for TOML and JSON"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/user/markdown.html#front-matter"
      reporter: jramsay
      stage: create
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/52007"
      description: |
        フロントマターはマークダウンドキュメントの先頭に入るメタデータで、[Jekyll](https://jekyllrb.com/docs/front-matter/)や[Hugo](https://gohugo.io/content-management/front-matter/)といった静的サイトジェネレーターでよく使われます。
        GitLabでリポジトリでマークダウンファイルをHTMLにレンダリングして表示する際、フロントマターはそのフォーマットを維持し、想定通りに表示されます。
        YAMLのフロントマターの区切り文字である(---)に加え、GitLabはTOMLの区切り文字(+++)、JSONの区切り文字(;;;)、
        任意の区切り文字、をサポートし、どのようなデータフォーマットに対してもサポートができるようになります。

        [Travis Miller](https://gitlab.com/travismiller)さんの貢献に感謝します！


    - name: "Auto DevOpsがグループセキュリティダッシュボードをサポート"
      name_en: "Auto DevOps support for Group Security Dashboard"
      available_in: [ultimate]
      documentation_link: "https://docs.gitlab.com/ee/topics/autodevops/"
      reporter: bikebilly
      stage: secure
      issue_url: "https://gitlab.com/gitlab-org/gitlab-ce/issues/54160"
      description: |
        [GitLab 11.5](https://about.gitlab.com/2018/11/22/gitlab-11-5-released/#group-security-dashboard){:target="_blank"}
        で
        [SAST](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html){:target="_blank"}
        の結果が表示される
        [Group Security Dashboard](https://docs.gitlab.com/ee/user/group/security_dashboard/){:target="_blank"}
        をリリースしました。

        11.6では最新の
        [SASTジョブ](https://docs.gitlab.com/ee/ci/examples/sast.html){:target="_blank"}
        定義に
        [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/){:target="_blank"}
        テンプレートをアップデートし、結果がグループセキュリティダッシュボードに完全に互換となりました。
        ユーザは同時に両方の機能を使うことができます。

        **注記:** 最新のSASTジョブ定義は
        [GitLab Runner](https://docs.gitlab.com/runner/){:target="_blank"}
        11.5以上が必須となります。より詳細な情報はこちらの
        [ブログ記事](/2018/12/06/gitlab-runner-update-required-to-use-auto-devops-and-sast/){:target="_blank"}
        に記載されています。

    - name: "Geoの改良点"
      name_en: "Geo improvements"
      available_in: [premium, ultimate]
      gitlab_com: false
      documentation_link: "https://docs.gitlab.com/ee/administration/geo/replication/configuration.html"
      reporter: akaemmerle
      issueboard_url: "https://gitlab.com/groups/gitlab-org/-/boards/823788?milestone_title=11.6&&label_name[]=Geo"
      description: |
        私たちは遠隔地で作業するチームのために、Geoの機能を継続的に改良することに注力しています。
        GitLab11.6での特筆すべき改良点は、

        - [Geoが絶えずリポジトリを再確認するように](https://gitlab.com/gitlab-org/gitlab-ee/issues/7347){:target="_blank"}
        - [ハッシュストレージのGA確定](https://gitlab.com/groups/gitlab-org/-/epics/75){:target="_blank"}

    - name: GitLab Runner 11.6
      name_en: GitLab Runner 11.6
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/runner"
      documentation_text: "Read through the documentation of GitLab Runner"
      description: |
        GitLab Runner11.6も併せてリリースします。GitLab Runnerはオープンソースプロジェクトで、
        CI/CDのジョブを実行し、結果をGitLabに返すところで利用されています。

        ##### 最も興味深い変更点:

        * [Docker executor:設定からDNSとDNSSearchとExtraHosts設定を利用](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1075){:target="_blank"}
        * [ドキュメントから無効なリンクを削除](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1085){:target="_blank"}
        * [k8sのオブジェクト名をDNS-1123に準拠したものに変更](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests/1105){:target="_blank"}

        全ての変更リストはGitLab Runnerの
        [CHANGELOG](https://gitlab.com/gitlab-org/gitlab-runner/blob/v11.6.0/CHANGELOG.md){:target="_blank"}
        で参照できます。

    - name: "Omnibusの改良点"
      name_en: "Omnibus improvements"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: "https://docs.gitlab.com/omnibus/"
      reporter: joshlambert
      team: distribution
      description: |
        - Postgresはメジャーバージョン名の付いたディレクトリ配下にインストールされるようになるため、メジャーバージョン内でのアップデートでは、データベースの再起動が必要なくなりました。
        - GitLabは [SSLを使ったRedisの接続(rediss://)](https://docs.gitlab.com/omnibus/settings/redis.html#using-secure-sockets-layer-ssl){:target="_blank"}をサポートします。
        - Ominibus-gitlabのコンテナイメージのsshd設定ファイルはデフォルトでGitプロトコルv2をサポートするようになります。
        - GitLab 11.6は、[Slack代替のオープンソース](https://mattermost.com/){:target="_blank"}である[Mattermost 5.5](https://mattermost.com/blog/mattermost-5-5-web-conferencing-integration-hacktoberfest-contributions-and-more/){:target="_blank"}を含んでおり、その最新リリースでは、幾つかのバグが修正され、パフォーマンスが向上しています。
        - `postgres` が9.6.11に、 `ruby` が2.5.3にアップデートされ、 `docker-distribution` がpartialsetの2.7.0でコミットされました。
        - `prometheus` が2.5.0に、 `prometheus-storage-migrator` が0.2.0に、 `postgres-exporter` が0.4.7に、 `pgbouncer-exporter` が0.0.4にアップデートされました。

    - name: "パフォーマンス向上について"
      name_en: "Performance improvements"
      available_in: [core, starter, premium, ultimate]
      performance_url: https://gitlab.com/groups/gitlab-org/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name%5B%5D=performance&milestone_title=11.6
      image_url: "/images/11_6/reactive-cache.png"
      reporter: multiple
      team: multiple
      description: |
        私たちは、毎回のリリースで全てのサイズのGitLabインスタンスに対して、GitLabのパフォーマンス改善に継続的に取り組んでいます。

        GitLab 11.6では、XMLのパースをNokogiriに変えることで`ReactiveCaching` workerプロセスのメモリ利用量を劇的に減少させました。
        また、マージリクエストのディスカッションエンドポイントのペイロード長を半分に圧縮しました。
        上記とその他の特筆すべきパフォーマンス向上は以下を含んでいます。

        - [各ディスカッションオブジェクトの依存関係数を減らすことで新規/現行のマージリクエストリクエストディスカッションのレンダリングパフォーマンス向上](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/22935)
        - [未使用のデータ削除でマージリクエストディスカッションエンドポイントのサイズ改善](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/23570)
        - [readme_urlを取得する際にN+1RPCコールを削除することで、プロジェクトAPIのパフォーマンス向上](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/23357)
        - [課題ボード初回画面表示時のパフォーマンス向上](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/23324)
        - [XMLパーサー変更による`ReactiveCaching` ワーカープロセスのパフォーマンス向上](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/23136)

        以下のグラフは、GitLab.comにGitLab 11.6を適用してからReactiveCaching`ワーカープロセスによるメモリ使用量が減少していることを表しています。

mvp:
  fullname: "Suzanne Hillman"
  gitlab: wispfox
  description: |
    Suzanneさんは、GitLabのアクセシビリティに関する課題の分類や検証を実施し、最新のGitLabの
    [Voluntary Product Accessibility Template](https://design.gitlab.com/accessibility/vpat){:target="_blank"}
    (VPAT)の評価に大きな貢献をしました。
    VPATはアクセシビリティ標準への準拠を評価し、アクセシビリティ改善に向けた最初の大きな一歩となります。

cover_img:
  image_url: "https://unsplash.com/photos/KGc9MaDmjtI"
  licence: Unsplash
  licence_url: "https://unsplash.com/license"

cta:
  # - title: "Join us for an upcoming event"
  #   link: "/events/"

barometer:
  reporter: ahanselka
  description: |
    マルチノード/HA構成の場合は、最新のGitLab 11.5から11.6へのアップグレードではダウンタイムは発生しません。
    アップグレードでのダウンタイムを発生させたくない場合は、
    [ダウンタイムレスアップグレードのドキュメント](https://docs.gitlab.com/ee/update/README.html#upgrading-without-downtime){:target="_blank"}
    を確認してください。

    シングルノード構成の場合は、
    [`ruby`のアップグレード](https://docs.gitlab.com/omnibus/update/gitlab_11_changes.html#116){:target="_blank"}
    のために、`unicorn`プロセスを再起動する間にダウンタイムが発生します。
    この再起動は、アップグレードプロセスの最後に自動的に実行されます。

    このリリースではマイグレーション、デプロイ後マイグレーションが必要です。
    大規模なマイグレーションが必要な場合は、バックグラウンドジョブとしてマイグレーションが実行されます。

    GitLab.comのマイグレーションには約25分、デプロイ後マイグレーションには合計で約2分かかりました。
    sidekiqのジョブで同期的に実行されるバックグラウンドマイグレーションは、完了するまでにおよそ15分かかりましたが、
    副作用は発生しないことが予想されていて、実際に発生しませんでした。

    GitLab Geoのユーザーは、
    [Geoのアップグレードのドキュメント](https://docs.gitlab.com/ee/administration/geo/replication/updating_the_geo_nodes.html){:target="_blank"}
    を確認してください。

deprecations:
  - feature_name: "GitLab 12.0からGitLab Geoのハッシュストレージが強制"
    feature_name_en: "GitLab Geo will enforce Hashed Storage in GitLab 12.0"
    due: Mar. 22, 2019.
    reporter: mkozono
    description: |
      Geoのセカンダリで発生する競合状態の問題を低減するため、GitLab Geoの
      [ハッシュストレージ](https://docs.gitlab.com/ee/administration/repository_storage_types.html#hashed-storage){:target="_blank"}
      を使用する必要があります。この問題の詳細は、
      [gitlab-ce#40970](https://gitlab.com/gitlab-org/gitlab-ce/issues/40970){:target="_blank"}
      に記載されます。

      11.5では、この要件がGeoのドキュメントに追加されました:
      [gitlab-ee#8053](https://gitlab.com/gitlab-org/gitlab-ee/issues/8053){:target="_blank"}

      11.6では、`sudo gitlab-rake gitlab:geo:check`コマンドでハッシュストレージが有効化されているかチェックし、
      すべてのプロジェクトを移行するようにしました:
      [gitlab-ee#8289](https://gitlab.com/gitlab-org/gitlab-ee/issues/8289){:target="_blank"}
      もしGeoを使用している場合は、なるべく早くこのチェックを実行し、移行をしてください。

      11.7では、"Admin Area › Geo › Nodes"ページに警告を表示する予定です。

      12.0では、Geoのハッシュストレージが強制される予定です:
      [gitlab-ee#86e90](https://gitlab.com/gitlab-org/gitlab-ee/issues/8690){:target="_blank"}

  - feature_name: "Omnibus GitLabでのPrometheus 1.xのサポート"
    feature_name_en: "Support for Prometheus 1.x in Omnibus GitLab"
    due: GitLab 12.0
    reporter: joshlambert
    description: |
      GitLab 11.4(2018年10月22日)からOmnibus GitLabに同梱されるPrometheus 1.0は非推奨となります。
      今後はPrometheus 2.0が同梱されますが、メトリクスのフォーマットは1.0と互換性がありません。
      既存のインスタンスは[同梱されるツール](https://docs.gitlab.com/omnibus/update/gitlab_11_changes.html#11-4){:target="_blank"}
      を使用して2.0へアップグレード、およびオプションでデータの移行ができます。

      GitLab 12.0では、Prometheus 2.0が使用されていない場合は自動的にアップグレードされます。
      その時に、Prometheus 1.0のデータ移行は行なわれないため、データは消去されます。

  - feature_name: "12.0でTLS v1.1がデフォルトで無効化"
    feature_name_en: "TLS v1.1 will be disabled by default in 12.0"
    due: GitLab 12.0
    reporter: joshlambert
    description: |
      セキュリティを向上させるため、GitLab 12.0から
      [TLS v1.1がデフォルトで無効化](https://docs.gitlab.com/omnibus/update/gitlab_11_changes.html#tls-v11-deprecation){:target="_blank"}
      されます。
      これにより、HeatbleedやSS 3.1標準への準拠などの問題が低減されます。

      TLS v1.1をただちに無効化するには、
      `gitlab.rb`の`nginx['ssl_protocols'] = "TLSv1.2"`を設定し、
      `gitlab-ctl reconfigure`を実行してください。