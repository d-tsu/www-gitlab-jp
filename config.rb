require 'extensions/breadcrumbs'
require 'lib/homepage'
require 'lib/mermaid'
require "thwait"
require 'lib/gitlab_jp/redirect'

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

activate :blog do |blog|
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  # blog.custom_collections = {
  #   categories: {
  #     link: '/blog/categories/{categories}/index.html',
  #     template: '/category.html'
  #   }
  # }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Proxy Comparison html and PDF pages
data.features.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}.html", "/templates/comparison.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }
  proxy "/devops-tools/pdfs/#{file_name}.html", '/devops-tools/pdfs/template.html', locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }, ignore: true
end

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  set :build_dir, 'public'
  set :base_url, '/www-gitlab-jp' # baseurl for GitLab Pages (project name) - leave empty if you're building a user/group website
  activate :minify_css
  activate :minify_javascript
  activate :minify_html
end

configure :development do
  activate :livereload
end

ignore '/templates/*'
ignore '/includes/*'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'
ignore '**/template.html'

GitlabJp::Redirect.each_for_middleman do |source, target|
  redirect source, to: target
end
