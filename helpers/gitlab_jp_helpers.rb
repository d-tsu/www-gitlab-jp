module GitlabJpHelpers
  def gtm_enabled?
    ENV['ENABLE_GTM'] == 'true'
  end

  def page_title
    if content_for?(:title)
      "#{yield_content :title} | GitLab.JP"
    elsif current_page.data.title
      "#{current_page.data.title} | GitLab.JP"
    else
      "GitLab.JP"
    end
  end

  def page_description
    if content_for?(:description)
      yield_content :description
    elsif current_page.data.description
      current_page.data.description
    else
      "GitLabは先進的なソフトウェア開発に必要な多くの機能を統合した製品です。"
    end
  end

  def full_path
    current_page.file_descriptor.relative_path
  end

  def edit_page_url
    "#{data.site.repo}blob/master/source/#{full_path}"
  end

  def contributing_url
    "#{data.site.repo}blob/master/CONTRIBUTING.md"
  end

  def canonical_url
    "#{data.site.url}#{current_page.url}"
  end
end